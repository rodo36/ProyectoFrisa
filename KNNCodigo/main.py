class codigo():

    def __init__(self):
        pass

    def algoritmo(self, parameter):
        import pandas as pd
        import numpy as np
        import winsound
        from normal import Normal
        from dataInput import Input
        from KNN import KNN
        from quimica import quimica
        # cargamos los datos de entrada usando un metodo de la clase Input
        path = (r'C:/Users/ECON Tech/Desktop/ProyectoFrisa/Data_train_val_frisa3.xlsx')
        input = Input()
        normal = Normal()
        knn = KNN()
        quimica = quimica()
        # Data train
        data_train = input.data(path, 'DataTrain')
        #Data val
        data_val = input.data(path, 'DataVal')
        #Concatenacion de data_train y data_val
        data_train_Val = pd.concat([data_train, data_val], axis=0)
        #Salidas train
        salidas_train = input.data(path, 'SalidasTrain')
        #Salidas val
        salidas_val = input.data(path, 'SalidasVal')
        #Concatenacion de salidas_train y salidas_val
        data_salidas_trainval = pd.concat([salidas_train, salidas_val], axis=0)
        ## Parametros para KNN
        k_muestras = 6
        ##_______Normalizamos los datos de entrada___________
        data_train_Val_norm = normal.normalize(data_train_Val)
        data_salidas_trainval_norm = normal.normalize(data_salidas_trainval)
        ##Separacion de data normalizada
        # Data train normalizado
        data_norm_train = data_train_Val_norm.iloc[0:len(data_train), :]
        # Data Val normalizado
        data_norm_val = data_train_Val_norm.iloc[len(data_train):len(data_train_Val_norm), :]
        # Salidas train normalizado
        salidas_norm_train = data_salidas_trainval_norm.iloc[0:len(data_train), :]
        # Salidas val normalizado
        salidas_norm_val = data_salidas_trainval_norm.iloc[len(data_train):len(data_train_Val_norm), :]
        ###algoritmo de machine learning KNN
        dataVal = data_norm_val.values  # data val
        dataTrain = data_norm_train.values  # data train
        #Distancias
        matDistancia = knn.distancia(dataVal, dataTrain)
        ##Indices y matriz ordenada
        disOrdenada, indice = knn.ordenar(dataVal, dataTrain, matDistancia)
        ##Funcion promedio como salida
        Indice_df = pd.DataFrame(indice)
        Prom = knn.promediOut(Indice_df, salidas_norm_train)
        ##Cuales son las muestras que se parecen a mi muestra a clasificar
        Indice_k = pd.DataFrame(indice).iloc[:, 0:k_muestras]
        ##Desnormalizar los datos
        result = normal.desnormalize(Prom, data_salidas_trainval)
        ##Parte quimica
        funcion = parameter['Func']
        if funcion == '1LF':
            row = quimica.calculo1LF(parameter, result)
            listalistas = []
            n = int(len(row))# / len(rows))  # int(len(row) / len(rows))
            listalistas.append(row)
            #listalistas = pd.DataFrame(np.array(row).reshape(len(rows), n), columns= quimica.titulos())
            listalistas = pd.DataFrame(np.array(row).reshape(1, n), columns= quimica.titulos_1LF())
            colnames_listalistas = pd.DataFrame(list(listalistas.columns))
            json = listalistas.to_json()
        elif funcion == 'ULF':
            row = quimica.calculoULF(parameter)
            listalistas = []
            n = int(len(row))# / len(rows))  # int(len(row) / len(rows))
            listalistas.append(row)
            #listalistas = pd.DataFrame(np.array(row).reshape(len(rows), n), columns= quimica.titulos())
            listalistas = pd.DataFrame(np.array(row).reshape(1, n), columns= quimica.titulos_ULF())
            colnames_listalistas = pd.DataFrame(list(listalistas.columns))
            json = listalistas.to_json()
        else:
            json = 0
        #listalistas.to_excel(r'C:/Users/ECON Tech/Desktop/ProyectoFrisa/Resultados_main.xlsx', engine='xlsxwriter')
        #print (listalistas)
        #Instruccion para que se emita un sonido indicando que la ejecucion del programa ha terminado"""
        winsound.Beep(1000,700)
        return (json)