class Input:
    def __init__(self):
        pass

    def data(self, path, sheet):
        import pandas as pd
        xls = pd.ExcelFile(path)
        x = pd.read_excel(xls, sheet)
        data = x.iloc[:,0:14]
        return data
