import pandas as pd
import numpy as np
from main import codigo
##Lectura de parametros para la estimacion quimica
Parametro_Ox_Al1 = 30
AlPostas_Quimica_Al = 97
# Constante de ajuste para la masa de oxidos
Cte_Al2O3 = 43.17 #76.659
Cte_SiO2 = -27.389 #3.154
Cte_CaO =  0 #42.83 #mas baja
Cte_MnO = 0
Cte_MgO = -21.101 #cambie esto el 17 de ago
# Lectura de la riqueza de la ferroaleacion
# Aluminio Postas/Al Briquetas
AlBrique_Quimica_Al = 97
# AlPostas_Quimica_Al = 97 lo puse arriba de data quimica
Al_brique_Quimica_Si = 0.1
Al_Postas_Quimica_Si = 0.1
# Aluminio en alambre para ajuste de la composicion quimica del acero
# Quimica de las adiciones
AlAlambre_AL = 100
# Cal Dolomitica Gruesa
CalDolomitica_QuimicaCaO = 40 #50
CalDolomitica_QuimicaMgO = 30
CalDolomitica_QuimicaMnO = 0
CalDolomitica_QuimicaAl2O3 = 1
CalDolomitica_QuimicaP2O5 = 0
CalDolomitica_QuimicaSiO = 2.5
CalDolomitica_QuimicaFeO = 0
# Cal Siderurgica fina
CalSiderurgica_fina_QuimicaCaO = 70# 90
CalSiderurgica_fina_QuimicaMgO = 2#2.5
CalSiderurgica_fina_QuimicaMnO = 0
CalSiderurgica_fina_QuimicaAl2O3 = 5
CalSiderurgica_fina_QuimicaAl = 0
CalSiderurgica_fina_QuimicaP2O5 = 0
CalSiderurgica_fina_QuimicaSiO = 2.5
CalSiderurgica_fina_QuimicaFe2O3 = 0.5
CalSiderurgica_fina_QuimicaFeO = 0
# Cal Siderurgica Gruesa
CalSiderurgica_Gruesa_QuimicaCaO = 70#90
CalSiderurgica_Gruesa_QuimicaMgO = 2#2.5
CalSiderurgica_Gruesa_QuimicaMnO = 0
CalSiderurgica_Gruesa_QuimicaAl2O3 = 0.5
CalSiderurgica_Gruesa_QuimicaP2O5 = 0
CalSiderurgica_Gruesa_QuimicaSiO = 2.5
CalSiderurgica_Gruesa_QuimicaFe2O3 = 0.5
CalSiderurgica_Gruesa_QuimicaAl = 0
CalSiderurgica_Gruesa_QuimicaFeO = 0
# FeSi Grado C
FeSi_GC_QuimicaCaO = 0
FeSi_GC_QuimicaMgO = 0
FeSi_GC_QuimicaMnO = 0
FeSi_GC_QuimicaAl2O3 = 0.5  # = FeSi_GC_QuimicaAl , es el contenido de Al
FeSi_GC_QuimicaP2O5 = 0
FeSi_GC_QuimicaSi = 74.3
FeSi_GC_QuimicaFeO = 0
FeSi_GC_QuimicaMn = 0
FeSi_GC_QuimicaP = 0.019 #0.019
# Silico Manganeso Estandar
SiMnSTD_QuimicaCaO = 0
SiMnSTD_QuimicaMgO = 0
SiMnSTD_QuimicaMn = 66
SiMnSTD_QuimicaAl2O3 = 0
SiMnSTD_QuimicaP2O5 = 0
SiMnSTD_QuimicaSiO = 0
SiMnSTD_QuimicaFeO = 0
SiMnSTD_QuimicaSi = 16.1
SiMnSTD_QuimicaP = 0.05  #0.16
# Leer Rendimientos de Ferroaleaciones
Rend_SI_SiMnSTD = 65
Rend_Mn_SiMnSTD = 97 #91
Rend_FeSi_GC = 75#70
# Leer Factores de conversion
Si_to_SiO2 = 2.14
Mn_to_MnO = 1.29
Al_to_Al2O3 = 1.89
FeT_to_FeO = 1.29
P_to_P2O5 = 2.29
# Lectura de aportes adicionales
# Composicion quimica de la escoria EAF % tipico de distribucion de los oxidos (debe ser ajustado en base a la estadistica de frisa)
# CaO
CaO_porc_statHF = 0.48
# MgO
MgO_porc_statHF = 0.12
# Al2O3
Al2O3_porc_statHF = 0.31
# SiO2
SiO2_porc_statHF = 0.003
# MnO
MnO_porc_statHF = 0.04
# FeO
FeO_porc_statHF = 0.4
# P2O5
P2O5_porc_statHF = 0.3
# Lectura de quimica tipica en porcentaje y kg de pase de arena fijos
# CaO
CaO_porc_EBT = 0
# MgO
MgO_porc_EBT = 0.47
# Al2O3
Al2O3_porc_EBT = 0
# SiO2
SiO2_porc_EBT = 0.405
# MnO
MnO_porc_EBT = 0
# FeO
FeO_porc_EBT = 0
# LECTURA Kilogramos de pase de arena de EBT
PaseArena_EBT_teorico = 50

class quimica():
    def __init__(self):
        pass

    def calculo1LF(self, parameter, result):
        #return parameter
        _1LF = []
        Ton_vaciado = parameter["Ton_vaciado"]
        # adiciones tap
        CalDolomitica_tap = float(parameter["CalDolomitica_1LF"])
        CalSiderurgica_gruesa_tap = float(parameter["CalSiderurgica_gruesa_1LF"])
        CalSiderurgica_fina_tap = float(parameter["CalSiderurgica_fina_1LF"])
        AlBriqueta_tap = float(parameter["AlBriqueta_1LF"])
        AlPosta_tap = float(parameter["AlPosta_1LF"])
        FerroSi_gradoC_tap = float(parameter["FerroSi_gradoC_1LF"])
        SiMnStd_gradoD_tap = float(parameter["SiMnStd_gradoD_1LF"])
        AlPostaBriquetas_tap = AlBriqueta_tap + AlPosta_tap  # =suma de cols 9+10
        # adiciones lf
        Parametro_Ox_Al = 30
        # Calculo de masas de oxidos y fluoruros en la escoria
        ##Oxidos producidos por adiciones en tapping
        # Calculo de masa de alumina producida por adiciones en tapping
        Mass_Al2O3_total_tap = (Cte_Al2O3 + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
                CalDolomitica_tap * CalDolomitica_QuimicaAl2O3 / 100) +
                                (CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaAl2O3 / 100) + (
                                        (FerroSi_gradoC_tap * FeSi_GC_QuimicaAl2O3 * (100-Rend_FeSi_GC) / 10000) * Al_to_Al2O3) +
                                (AlPosta_tap * (100 - Parametro_Ox_Al) * AlPostas_Quimica_Al / 10000 * Al_to_Al2O3) + (
                                        AlBriqueta_tap * (
                                        100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3))
        # CaO producidas por adiciones de tapping
        Mass_CaO_total_tap = (Cte_CaO + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaCaO / 100) + (
                CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaCaO / 100) +
                        (CalDolomitica_tap * CalDolomitica_QuimicaCaO / 100))
        # Si producido por adiciones de tapping
        Mass_SiO2_total_tap = (Cte_SiO2 + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaSiO / 100) + (
                CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaSiO / 100) +
                        (CalDolomitica_tap * CalDolomitica_QuimicaSiO / 100) + (((
                        FerroSi_gradoC_tap * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                        ((SiMnStd_gradoD_tap * SiMnSTD_QuimicaSi * (100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) + (
                                AlPosta_tap * (
                                100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2) +
                        (AlBriqueta_tap * (100 - Parametro_Ox_Al) * Al_brique_Quimica_Si / 10000 * Si_to_SiO2))
        # MnO producido por adiciones de tapping
        Mass_MnO_total_tap = (
                Cte_MnO + (SiMnStd_gradoD_tap * (100 - Rend_Mn_SiMnSTD) * SiMnSTD_QuimicaMn / 10000 * Mn_to_MnO))
        # MgO producido por adiciones de tapping
        Mass_MgO_total_tap = (Cte_MgO + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaMgO / 100) + (
                CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaMgO / 100) +
                        (CalDolomitica_tap * CalDolomitica_QuimicaMgO / 100))
        # P2O5 producido por adiuciones en tapping
        Mass_P2O5_total_tap = ((FerroSi_gradoC_tap * FeSi_GC_QuimicaP * (100- Rend_FeSi_GC) / 10000 * P_to_P2O5) + (
                SiMnStd_gradoD_tap * SiMnSTD_QuimicaP * (100- Rend_SI_SiMnSTD) / 10000 * P_to_P2O5))
        Mass_Fe2O3_total_tap = ((CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaFe2O3 / 100) + (
                CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaFe2O3 / 100))
        _1LF.append(Mass_Al2O3_total_tap)
        _1LF.append(Mass_CaO_total_tap)
        _1LF.append(Mass_SiO2_total_tap)
        _1LF.append(Mass_MnO_total_tap)
        _1LF.append(Mass_MgO_total_tap)
        _1LF.append(Mass_P2O5_total_tap)
        _1LF.append(Mass_Fe2O3_total_tap)
        ##Escoria total
        Escoria_total_tap = Mass_Al2O3_total_tap + Mass_CaO_total_tap + Mass_SiO2_total_tap + Mass_MnO_total_tap + Mass_MgO_total_tap + Mass_P2O5_total_tap + Mass_Fe2O3_total_tap
        _1LF.append(Escoria_total_tap)
        ##Porcentajes de oxidos primera muestra lf1
        Porc_Al2O3_tap = (Mass_Al2O3_total_tap * 100 / Escoria_total_tap)
        Porc_CaO_tap = (Mass_CaO_total_tap * 100 / Escoria_total_tap)
        Porc_SiO_tap = (Mass_SiO2_total_tap * 100 / Escoria_total_tap)
        Porc_MnO_tap = (Mass_MnO_total_tap * 100 / Escoria_total_tap)
        Porc_MgO_tap = (Mass_MgO_total_tap * 100 / Escoria_total_tap)
        Porc_P2O5_tap = (Mass_P2O5_total_tap * 100 / Escoria_total_tap)
        Porc_Fe2O3_tap = (Mass_Fe2O3_total_tap * 100 / Escoria_total_tap)
        BA3_tap = Porc_CaO_tap / (Porc_SiO_tap + Porc_Al2O3_tap)
        Relacion_CaOAl2O3_Tap = Porc_CaO_tap / Porc_Al2O3_tap

        _1LF.append(Porc_Al2O3_tap)
        _1LF.append(Porc_CaO_tap)
        _1LF.append(Porc_SiO_tap)
        _1LF.append(Porc_MnO_tap)
        _1LF.append(Porc_MgO_tap)
        _1LF.append(Porc_P2O5_tap)
        _1LF.append(Porc_Fe2O3_tap)
        _1LF.append(BA3_tap)
        _1LF.append(Relacion_CaOAl2O3_Tap)
        ## Lectura de salidas del KNN
        CalDol_knn = result.iloc[:, 0].values[0]
        CalSiderGruesa_knn = result.iloc[:, 1].values[0]
        CalSiderFina_knn = result.iloc[:, 2].values[0]
        AlBriqueta_knn = result.iloc[:, 3].values[0]
        AlPosta_knn = result.iloc[:, 4].values[0]
        _1LF.append(CalDol_knn)
        _1LF.append(CalSiderGruesa_knn)
        _1LF.append(CalSiderFina_knn)
        _1LF.append(AlBriqueta_knn)
        _1LF.append(AlPosta_knn)

        return(_1LF)

    def calculoULF(self,parameter):
        ULF = []
        Mass_Al2O3_total_tap = float(parameter["Mass_Al2O3_total_tap"])
        Mass_CaO_total_tap = float(parameter["Mass_CaO_total_tap"])
        Mass_SiO2_total_tap = float(parameter["Mass_SiO2_total_tap"])
        Mass_MnO_total_tap = float(parameter["Mass_MnO_total_tap"])
        Mass_MgO_total_tap = float(parameter["Mass_MgO_total_tap"])
        Mass_P2O5_total_tap = float(parameter["Mass_P2O5_total_tap"])
        Mass_Fe2O3_total_tap = float(parameter["Mass_Fe2O3_total_tap"])
        CalDol_knn = float(parameter["CalDol_knn"])
        CalSiderGruesa_knn = float(parameter["CalSiderGruesa_knn"])
        CalSiderFina_knn = float(parameter["CalSiderFina_knn"])
        AlBriqueta_knn = float(parameter["AlBriqueta_knn"])
        AlPosta_knn = float(parameter["AlPosta_knn"])
        FerroSi_gradoC_lf = float(parameter["FerroSi_gradoC_lf"])
        SiliMgStd_gradoD_lf = float(parameter["SiliMgStd_gradoD_lf"])
        Alalambre_lf = float(parameter["Alalambre_lf"])
        Parametro_Ox_Al = 30  # ULFs[i][38] #capacidad del al que al añadirlo , se oxida    
        ## Balance posterior a la adicion
        # Calculo de masa de alumina producida por adiciones en lf
        ##Oxidos producidos por adicionse de lf2
        Escoria_TotalWeight = 0.0
        Mass_Al2O3_total_lf2 = (Cte_Al2O3 + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
                CalDol_knn * CalDolomitica_QuimicaAl2O3 / 100) +
                                (CalSiderFina_knn * CalSiderurgica_fina_QuimicaAl2O3 / 100) + (
                                        (FerroSi_gradoC_lf * FeSi_GC_QuimicaAl2O3 * (100-Rend_FeSi_GC)  / 10000) * Al_to_Al2O3) +
                                (AlPosta_knn * (100 - Parametro_Ox_Al) * AlPostas_Quimica_Al / 10000 * Al_to_Al2O3) + (
                                        AlBriqueta_knn * (
                                        100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3)
                                + (Alalambre_lf * (100 - Parametro_Ox_Al) * 100 / 10000 * Al_to_Al2O3)
                                + Mass_Al2O3_total_tap)
        Mass_CaO_total_lf2 = (Cte_CaO + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaCaO / 100) + (
                CalSiderFina_knn * CalSiderurgica_fina_QuimicaCaO / 100) +
                        (CalDol_knn * CalDolomitica_QuimicaCaO / 100) + Mass_CaO_total_tap)
        Mass_SiO2_total_lf2 = (Cte_SiO2 + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaSiO / 100) + (
                CalSiderFina_knn * CalSiderurgica_fina_QuimicaSiO / 100) +
                        (CalDol_knn * CalDolomitica_QuimicaSiO / 100) + (((
                        FerroSi_gradoC_lf * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                        ((SiliMgStd_gradoD_lf * SiMnSTD_QuimicaSi * (
                                100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) + (
                                AlPosta_knn * (
                                100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2) +
                        (AlBriqueta_knn * (100 - Parametro_Ox_Al) * Al_brique_Quimica_Si / 10000 * Si_to_SiO2) +
                        Mass_SiO2_total_tap)
        Mass_MnO_total_lf2 = (
                Cte_MnO + (SiliMgStd_gradoD_lf * (100 - Rend_Mn_SiMnSTD) * SiMnSTD_QuimicaMn / 10000 * Mn_to_MnO) +
                Mass_MnO_total_tap)
        Mass_MgO_total_lf2 = (Cte_MgO + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaMgO / 100) + (
                CalSiderFina_knn * CalSiderurgica_fina_QuimicaMgO / 100) +
                        (CalDol_knn * CalDolomitica_QuimicaMgO / 100) + Mass_MgO_total_tap)
        Mass_P2O5_total_lf2 = ((FerroSi_gradoC_lf * FeSi_GC_QuimicaP * (100-Rend_FeSi_GC) / 10000 * P_to_P2O5) + (
                SiliMgStd_gradoD_lf * SiMnSTD_QuimicaP * (100-Rend_SI_SiMnSTD) / 10000 * P_to_P2O5) + Mass_P2O5_total_tap)
        Mass_Fe2O3_total_lf2 = ((CalSiderFina_knn * CalSiderurgica_fina_QuimicaFe2O3 / 100) + (
                CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaFe2O3 / 100) + Mass_Fe2O3_total_tap)
        ULF.append(Mass_Al2O3_total_lf2)
        ULF.append(Mass_CaO_total_lf2)
        ULF.append(Mass_SiO2_total_lf2)
        ULF.append(Mass_MnO_total_lf2)
        ULF.append(Mass_MgO_total_lf2)
        ULF.append(Mass_P2O5_total_lf2)
        ULF.append(Mass_Fe2O3_total_lf2)
        ##Escoria total
        Escoria_total_lf2 = Mass_Al2O3_total_lf2 + Mass_CaO_total_lf2 + Mass_SiO2_total_lf2 + Mass_MnO_total_lf2 + Mass_MgO_total_lf2 + Mass_P2O5_total_lf2 + Mass_Fe2O3_total_lf2
        ULF.append(Escoria_total_lf2)
        ##Porcentajes de oxidos primera muestra lf
        Porc_Al2O3_lf2 = ((Mass_Al2O3_total_lf2) * 100 / Escoria_total_lf2)
        Porc_CaO_lf2 = ((Mass_CaO_total_lf2) * 100 / Escoria_total_lf2)
        Porc_SiO_lf2 = ((Mass_SiO2_total_lf2) * 100 / Escoria_total_lf2)
        Porc_MnO_lf2 = ((Mass_MnO_total_lf2) * 100 / Escoria_total_lf2)
        Porc_MgO_lf2 = ((Mass_MgO_total_lf2) * 100 / Escoria_total_lf2)
        Porc_P2O5_lf2 = ((Mass_P2O5_total_lf2) * 100 / Escoria_total_lf2)
        Porc_Fe2O3_lf2 = ((Mass_Fe2O3_total_lf2 * 100) / Escoria_total_lf2)
        ##Recalculo de MnO
        Cte_MnO_Corr1 = 0.07212
        Cte_MnO_Corr2 = 0.03344
        Porc_MnO_corregido = Porc_MnO_lf2 * Cte_MnO_Corr1 + Cte_MnO_Corr2
        Mass_MnO_lf2_corregida = Porc_MnO_corregido * Escoria_total_lf2 / 100
        Mass_MnO_corregida_new = Mass_MnO_total_lf2 - Mass_MnO_lf2_corregida
        Mass_Escoria_lf2_new = Mass_Al2O3_total_lf2 + Mass_CaO_total_lf2 + Mass_SiO2_total_lf2 + Mass_MnO_corregida_new + Mass_MgO_total_lf2 + Mass_P2O5_total_lf2 + Mass_Fe2O3_total_lf2
        # Porcentajes de oxidos corregidos por MnO
        Porc_Al2O3_lf2_new = ((Mass_Al2O3_total_lf2) * 100 / Mass_Escoria_lf2_new)
        Porc_CaO_lf2_new = ((Mass_CaO_total_lf2) * 100 / Mass_Escoria_lf2_new)
        Porc_SiO_lf2_new = ((Mass_SiO2_total_lf2) * 100 / Mass_Escoria_lf2_new)
        Porc_MnO_lf2_new = ((Mass_MnO_total_lf2) * 100 / Mass_Escoria_lf2_new)
        Porc_MgO_lf2_new = ((Mass_MgO_total_lf2) * 100 / Mass_Escoria_lf2_new)
        Porc_P2O5_lf2_new = ((Mass_P2O5_total_lf2) * 100 / Mass_Escoria_lf2_new)
        Porc_Fe2O3_lf2_new = ((Mass_Fe2O3_total_lf2 * 100) / Mass_Escoria_lf2_new)
        ULF.append(Porc_Al2O3_lf2_new)  # 16.80
        ULF.append(Porc_CaO_lf2_new)  # 54.37
        ULF.append(Porc_SiO_lf2_new)
        ULF.append(Porc_MnO_lf2_new)
        ULF.append(Porc_MgO_lf2_new)
        ULF.append(Porc_P2O5_lf2_new)
        ULF.append(Porc_Fe2O3_lf2_new)
        ##Optmizacion
        # f = float(Escoria_TotalFeO)
        Escoria_TotalMassAl2O3 = Mass_Al2O3_total_lf2
        Escoria_TargetAl2O3 = 30
        Escoria_TotalCaO = Porc_CaO_lf2
        Escoria_TotalMgO = Porc_MgO_lf2
        Escoria_TotalSiO2 = Porc_SiO_lf2
        Escoria_TotalAl2O3 = Porc_Al2O3_lf2
        Escoria_Total_Fe2O3 = Porc_Fe2O3_lf2
        Escoria_AlgranallaAddition = AlPosta_knn + AlBriqueta_knn
        Escoria_WeightAddition = Escoria_total_lf2  # Peso total de escoria
        Mass_Al2O3_Adicionales = Mass_Al2O3_total_lf2
        CalSiderVaciado_MassAl2O3 = CalSiderurgica_Gruesa_QuimicaAl2O3 + CalSiderurgica_fina_QuimicaAl2O3
        Escoria_TotalMassMgO = Mass_MgO_total_lf2
        Escoria_TotalMassCaO = Mass_CaO_total_lf2
        Escoria_TotalMassAl2O3 = Mass_Al2O3_total_lf2
        Escoria_TotalMassSiO2 = Mass_SiO2_total_lf2
        Escoria_TotalMassMnO = Mass_MnO_total_lf2
        Escoria_TotalMassFe2O3 = Mass_Fe2O3_total_lf2
        Escoria_TargetAl2O3 = 18  # 20
        Escoria_TargetBA4 = 2  # 1.8
        Escoria_TargetCaO = 56  # 54
        CalSiderurgica_QuimicaCaO = CalSiderurgica_Gruesa_QuimicaCaO
        Escoria_LimeAddition = CalSiderGruesa_knn + CalSiderFina_knn
        # basicidad final
        Escoria_BA4 = (Escoria_TotalCaO + Escoria_TotalMgO) / (Escoria_TotalSiO2 + Escoria_TotalAl2O3)
        Escoria_Al2O3Tolerance = 0.1  # 0.1
        Escoria_AlGranallaStep = 0.1
        Escoria_DeltaAl2O3 = 2 * float(Escoria_Al2O3Tolerance)  # 0.2
        MAX_ITER = 10000
        Iter = 0
        Iter2 = 0
        # Escoria_AlgranallaAddition = Mass_Al2O3_Al_ALAM01 + Mass_AlPosta
        while ((abs(Escoria_DeltaAl2O3) > Escoria_Al2O3Tolerance) and (Escoria_AlgranallaAddition > 0) and (
            Escoria_LimeAddition > 0) and Iter <= MAX_ITER):
                Iter = Iter + 1
                while ((abs(Escoria_DeltaAl2O3) > Escoria_Al2O3Tolerance) and (Escoria_AlgranallaAddition > 0) and (Escoria_LimeAddition > 0) and Iter2 <= MAX_ITER):
                        Iter2 = Iter2 + 1
                        Escoria_AlgranallaAddition = Escoria_AlgranallaAddition - Escoria_AlGranallaStep
                        if (Escoria_DeltaAl2O3 >= Escoria_Al2O3Tolerance):
                                Escoria_AlgranallaAddition = Escoria_AlgranallaAddition - Escoria_AlGranallaStep
                                if (Escoria_AlgranallaAddition < 0):
                                        Escoria_AlgranallaAddition = 0
                                Escoria_TotalWeight = float(Escoria_TotalWeight) - float(Escoria_AlGranallaStep)
                        else:
                                if (Escoria_DeltaAl2O3 <= ((-1) * Escoria_Al2O3Tolerance)):
                                        Escoria_AlgranallaAddition = Escoria_AlgranallaAddition + Escoria_AlGranallaStep
                                        Escoria_TotalWeight = Escoria_WeightAddition + Escoria_AlGranallaStep
                # Total weights of additions
                        AlGranalla_MassAl2O3 = (Cte_Al2O3 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
                        CalDol_knn * CalDolomitica_QuimicaAl2O3 / 100) +
                                        + ((FerroSi_gradoC_lf * FeSi_GC_QuimicaAl2O3 * (100- Rend_FeSi_GC ) / 10000) * Al_to_Al2O3) +
                                        + (Escoria_AlgranallaAddition * (
                                                100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3)
                                        + (Alalambre_lf * (100 - Parametro_Ox_Al) * 100 / 10000 * Al_to_Al2O3)
                                        )
                        AlGranalla_MassSiO2 = (Cte_SiO2 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaSiO / 100) +
                                        (CalDol_knn * CalDolomitica_QuimicaSiO / 100) + (((
                                FerroSi_gradoC_lf * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                                        ((SiliMgStd_gradoD_lf * SiMnSTD_QuimicaSi * (
                                                100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) +
                                        (Escoria_AlgranallaAddition * (
                                                100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2))
                        Escoria_TotalMassAl2O3 = Mass_Al2O3_total_tap + AlGranalla_MassAl2O3
                        Escoria_TotalMassSiO2 = Mass_SiO2_total_tap + AlGranalla_MassSiO2
                        Escoria_WeightAddition = (
                                Escoria_TotalMassAl2O3 + Escoria_TotalMassSiO2 + Mass_CaO_total_lf2 + Mass_MnO_total_lf2 + Mass_MgO_total_lf2 + Mass_Fe2O3_total_lf2 + Mass_P2O5_total_lf2)
                        # Final estimated slag composition with additions
                        Escoria_TotalMgO = float(100) * float(Escoria_TotalMassMgO) / float(Escoria_WeightAddition)
                        Escoria_TotalCaO = float(100) * float(Escoria_TotalMassCaO) / float(Escoria_WeightAddition)
                        Escoria_TotalAl2O3 = float(100) * float(Escoria_TotalMassAl2O3) / float(Escoria_WeightAddition)
                        C = float(Escoria_TotalAl2O3)
                        Escoria_TotalSiO2 = float(100) * float(Escoria_TotalMassSiO2) / float(Escoria_WeightAddition)
                        Escoria_TotalMnO = float(100) * float(Escoria_TotalMassMnO) / float(Escoria_WeightAddition)
                        Escoria_Total_Fe2O3 = 100 * Escoria_TotalMassFe2O3 / Escoria_WeightAddition
                        Escoria_Total_P2O5 = 100 * Mass_P2O5_total_lf2 / Escoria_WeightAddition
                        # Basicity:
                        Escoria_BA4 = (float(Escoria_TotalCaO) + float(Escoria_TotalMgO)) / (
                                float(Escoria_TotalSiO2) + float(Escoria_TotalAl2O3))
                        Escoria_DeltaAl2O3 = float(Escoria_TotalAl2O3) - float(Escoria_TargetAl2O3)
                        a = 1
                a = float(Escoria_AlgranallaAddition)
                B = float(Escoria_LimeAddition)
                C = float(a)
                # CaO that meets target BA4
                Escoria_CaoContent = Escoria_TargetCaO  # float(Escoria_BA4) * (float(Escoria_TotalSiO2) + float(Escoria_TargetAl2O3)) - float(Escoria_TotalMgO)
                Escoria_DeltaCaO = float(Escoria_CaoContent) - float(Escoria_TotalCaO)
                Escoria_MassCaOAddition = float(Escoria_TotalWeight) * (float(Escoria_DeltaCaO) / float(100))
                Escoria_FinalLimeAddition = float(Escoria_MassCaOAddition) * float(100) / float(CalSiderurgica_QuimicaCaO)
                Escoria_LimeAddition = float(Escoria_LimeAddition) + float(Escoria_FinalLimeAddition)
                Escoria_TotalWeight = float(Escoria_WeightAddition) + float(Escoria_FinalLimeAddition)
                Escoria_TotalMassCaO = float(Escoria_TotalMassCaO) + float(Escoria_MassCaOAddition)
                # Si Lime addition <0 -> lime addition =0
                if Escoria_LimeAddition < 0:
                        Escoria_LimeAddition = 0
                LimeAddition_MassAl2O3 = (Cte_Al2O3 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
                        CalDol_knn * CalDolomitica_QuimicaAl2O3 / 100) +
                                        + ((FerroSi_gradoC_lf * FeSi_GC_QuimicaAl2O3 * (100- Rend_FeSi_GC)/ 10000) * Al_to_Al2O3) +
                                        + (Escoria_AlgranallaAddition * (
                                                100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3)
                                        + (Alalambre_lf * (
                                        100 - Parametro_Ox_Al) * 100 / 10000 * Al_to_Al2O3) + Mass_Al2O3_total_tap)
                LimeAddition_MassCaO = (Cte_CaO + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaCaO / 100) +
                                        (CalDol_knn * CalDolomitica_QuimicaCaO / 100) + Mass_CaO_total_tap)
                LimeAddition_MassSiO2 = (Cte_SiO2 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaSiO / 100) +
                                        (CalDol_knn * CalDolomitica_QuimicaSiO / 100) + (((
                                FerroSi_gradoC_lf * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                                        ((SiliMgStd_gradoD_lf * SiMnSTD_QuimicaSi * (
                                                100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) +
                                        (Escoria_AlgranallaAddition * (
                                                100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2) +
                                        Mass_SiO2_total_tap)
                LimeAddition_MassMgO = (Cte_MgO + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaMgO / 100) +
                                        (CalDol_knn * CalDolomitica_QuimicaMgO / 100) + Mass_MgO_total_tap)
                LimeAddition_MassP2O5 = Mass_P2O5_total_lf2
                LimeAddition_MassFe2O3 = (
                        (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaFe2O3 / 100) + Mass_Fe2O3_total_tap)
                LimeAddition_MassMnO = Mass_MnO_total_lf2
                EscoriaMass_CalAjustado = (
                        LimeAddition_MassAl2O3 + LimeAddition_MassCaO + LimeAddition_MassSiO2 + LimeAddition_MassMgO + LimeAddition_MassP2O5 + LimeAddition_MassFe2O3 + LimeAddition_MassMnO)
                # Final estimated slag composition with additions:
                Escoria_TotalMgO = float(100) * float(LimeAddition_MassMgO) / float(EscoriaMass_CalAjustado)
                Escoria_TotalCaO = float(100) * float(LimeAddition_MassCaO) / float(EscoriaMass_CalAjustado)
                Escoria_TotalAl2O3 = float(100) * float(LimeAddition_MassAl2O3) / float(EscoriaMass_CalAjustado)
                Escoria_TotalSiO2 = float(100) * float(LimeAddition_MassSiO2) / float(EscoriaMass_CalAjustado)
                Escoria_TotalMnO = float(100) * float(LimeAddition_MassMnO) / float(EscoriaMass_CalAjustado)
                Escoria_Total_Fe2O3 = 100 * LimeAddition_MassFe2O3 / EscoriaMass_CalAjustado
                Escoria_Total_P2O5 = 100 * LimeAddition_MassP2O5 / EscoriaMass_CalAjustado
                # Basicity:
                Escoria_BA4 = (float(Escoria_TotalCaO) + float(Escoria_TotalMgO)) / (
                        float(Escoria_TotalSiO2) + float(Escoria_TotalAl2O3))
                Escoria_DeltaAl2O3 = float(Escoria_TotalAl2O3) - float(Escoria_TargetAl2O3)
        #Basicidad ternaria 
        Escoria_BA3 = Escoria_TotalCaO / (Escoria_TotalAl2O3 + Escoria_TotalSiO2)
        #Relación CaO/Al2O3
    Relacion_CaOAl2O3 = Escoria_TotalCaO / Escoria_TotalAl2O3



        ULF.append(Escoria_TotalMassMgO)
        ULF.append(Escoria_TotalMassCaO)
        ULF.append(Escoria_TotalMassAl2O3)
        ULF.append(Escoria_TotalMassSiO2)
        ULF.append(Escoria_TotalMassMnO)
        ULF.append(Escoria_TotalMassFe2O3)
        ULF.append(Mass_P2O5_total_lf2)

        ULF.append(LimeAddition_MassMgO)
        ULF.append(LimeAddition_MassCaO)
        ULF.append(LimeAddition_MassAl2O3)
        ULF.append(LimeAddition_MassSiO2)
        ULF.append(LimeAddition_MassMnO)
        ULF.append(LimeAddition_MassFe2O3)
        ULF.append(LimeAddition_MassP2O5)

        ULF.append(Escoria_TotalMgO)
        ULF.append(Escoria_TotalCaO)
        ULF.append(Escoria_TotalAl2O3)
        ULF.append(Escoria_TotalSiO2)
        ULF.append(Escoria_TotalMnO)
        ULF.append(Escoria_Total_Fe2O3)
        ULF.append(Escoria_Total_P2O5)
        ULF.append(Escoria_BA4)
        ULF.append(Escoria_BA3)
        ULF.append(Relacion_CaOAl2O3)
        a = Escoria_AlgranallaAddition
        ULF.append(a)
        B = Escoria_LimeAddition
        ULF.append(B)
        return (ULF)
    
    def titulos_1LF(self):
        titulos = (['Mass_Al2O3_total_tap', 'Mass_CaO_total_ta', 'Mass_SiO2_total_tap', 'Mass_MnO_total_tap',
            'Mass_MgO_total_tap', 'Mass_P2O5_total_tap', 'Mass_Fe2O3_total_tap', 'Escoria_total_tap',
            'Porc_Al2O3_tap', 'Porc_CaO_tap', 'Porc_SiO_tap', 'Porc_MnO_tap', 'Porc_MgO_tap', 'Porc_P2O5_tap',
            'Porc_Fe2O3_tap', 'BA3_tap','Relacion_CaOAl2O3_Tap',
            'CalDol_knn', 'CalSiderGruesa_knn', 'CalSiderFina_knn', 'AlBriqueta_knn', 'AlPosta_knn'])
        return titulos



    def titulos_ULF(self):
        titulos = (['Mass_Al2O3_total_lf2', 'Mass_CaO_total_lf2', 'Mass_SiO2_total_lf2', 'Mass_MnO_total_lf2',
        'Mass_MgO_total_lf2',
        'Mass_P2O5_total_lf2', 'Mass_Fe2O3_total_lf2',
        'Escoria_total_lf2',
        'Porc_Al2O3_lf2', 'Porc_CaO_lf2', 'Porc_SiO_lf2', 'Porc_MnO_lf2', 'Porc_MgO_lf2', 'Porc_P2O5_lf2',
        'Porc_Fe2O3_lf2',
        'Escoria_totalMgO_enAl', 'Escoria_TotalMassCaO_enAl', 'Escoria_TotalMassAl2O3_enAl',
        'Escoria_TotalMassSiO2_enAl',
        'Escoria_TotalMassMnO_enAl', 'Escoria_TotalMassFe2O3_enAl', 'Escoria_totalMassP2O5lf2_enAl',
        'Escoria_totalMgO_enCa', 'Escoria_TotalMassCaO_enCa', 'Escoria_TotalMassAl2O3_enCa',
        'Escoria_TotalMassSiO2_enCa',
        'Escoria_TotalMassMnO_enCa', 'Escoria_TotalMassFe2O3_enCa', 'Escoria_totalMassP2O5_enCa',
        'Escoria_TotalMgO', 'Escoria_TotalCaO', 'Escoria_TotalAl2O3', 'Escoria_TotalSiO2', 'Escoria_TotalMnO',
        'Escoria_Total_Fe2O3', 'Escoria_Total_P2O5', 'Escoria_BA4',,'Escoria_BA3', 'Relacion_CaOAl2O3' 'Escoria_AlgranallaAdition',
        'EscoriaLimeAddition'])
        return titulos
