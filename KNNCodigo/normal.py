import pandas as pd
class Normal:
    def __init__(self):
        pass

    def normalize(self, data):
        result = data.copy()
        for feature_name in data.columns:
            max_value = data[feature_name].max()
            min_value = data[feature_name].min()
            result[feature_name] = (data[feature_name] - min_value) / (max_value - min_value)
        return result

    def desnormalize(self, Prom, dataSal):
        res_calDol = []
        res_calSiderGruesa = []
        res_calSiderFina = [] 
        res_AluminioBriqueta = []
        res_AluminioPosta = []
        for i in range(0, len(Prom)):
            df2 = Prom.values  # SalidaEscoriaKNN  # dataframe que quiero desnormalizar
            res1 = ((dataSal['CalDol_LF'].max() - dataSal['CalDol_LF'].min()) *
                    df2[i][0]) + dataSal['CalDol_LF'].min()
            res_calDol.append(res1)
            res2 = ((dataSal['CalSiderGruesa_LF'].max() - dataSal['CalSiderGruesa_LF'].min()) *
                    df2[i][1]) + dataSal['CalSiderGruesa_LF'].min()
            res_calSiderGruesa.append(res2)
            res22 = ((dataSal['CalSiderFina_LF'].max() - dataSal['CalSiderFina_LF'].min()) *
                    df2[i][2]) + dataSal['CalSiderFina_LF'].min()
            res_calSiderFina.append(res22)
            res3 = ((dataSal['AlBriqueta_LF'].max() - dataSal[
                'AlBriqueta_LF'].min()) * df2[i][3]) + dataSal['AlBriqueta_LF'].min()
            res_AluminioBriqueta.append(res3)
            res33 = ((dataSal['AlPosta_LF'].max() - dataSal[
                'AlPosta_LF'].min()) * df2[i][4]) + dataSal['AlPosta_LF'].min()
            res_AluminioPosta.append(res33)
        ##Resultados
        result = pd.concat([pd.DataFrame(res_calDol), pd.DataFrame(res_calSiderGruesa), pd.DataFrame(res_calSiderFina),
                                pd.DataFrame(res_AluminioBriqueta), pd.DataFrame(res_AluminioPosta)],
                                axis=1)  # son mis salidas del algoritmo KNN
        result.columns = ['Cal Dolomitica LF', 'Cal Sider Gruesa LF', 'Cal Sider Fina LF', 'Aluminio Briqueta LF',
                            'Aluminio Posta Lf']
        return result