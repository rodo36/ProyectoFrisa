from flask import Flask, jsonify, request


app = Flask(__name__)

@app.route('/calculo1LF', methods=['POST'])
def codigo1LF():
    #print(request.json)
    parameter = {
        "bool": request.json['bool'],
        "Func": request.json['Func'],
        "Ton_vaciado" : request.json['Ton_vaciado'],
        "CalDolomitica_1LF" : request.json['CalDolomitica_1LF'],
        "CalSiderurgica_gruesa_1LF" : request.json['CalSiderurgica_gruesa_1LF'],
        "CalSiderurgica_fina_1LF" : request.json['CalSiderurgica_fina_1LF'],
        "AlBriqueta_1LF" : request.json['AlBriqueta_1LF'],
        "AlPosta_1LF" : request.json['AlPosta_1LF'],
        "FerroSi_gradoC_1LF" : request.json['FerroSi_gradoC_1LF'],
        "SiMnStd_gradoD_1LF" : request.json['SiMnStd_gradoD_1LF']
        }
    val = parameter["bool"]
    if val == "True":
        from main import codigo
        cd = codigo()
        return jsonify({"message":'Datos guardados exitosamente', "resultados":cd.algoritmo(parameter)})
    else:
        return jsonify({"message":'Se rechazo la solicitud'})
    return parameter

@app.route('/calculoULF', methods=['POST'])
def codigoULF():
    #print(request.json)
    parameter = {
        "bool": request.json['bool'],
        "Func": request.json['Func'],
        "Mass_Al2O3_total_tap" : request.json['Mass_Al2O3_total_tap'],
        "Mass_CaO_total_tap" : request.json['Mass_CaO_total_tap'],
        "Mass_SiO2_total_tap" : request.json['Mass_SiO2_total_tap'],
        "Mass_MnO_total_tap" : request.json['Mass_MnO_total_tap'],
        "Mass_MgO_total_tap" : request.json['Mass_MgO_total_tap'],
        "Mass_P2O5_total_tap" : request.json['Mass_P2O5_total_tap'],
        "Mass_Fe2O3_total_tap" : request.json['Mass_Fe2O3_total_tap'],
        "CalDol_knn" : request.json['CalDol_knn'],
        "CalSiderGruesa_knn" : request.json['CalSiderGruesa_knn'],
        "CalSiderFina_knn" : request.json['CalSiderFina_knn'],
        "AlBriqueta_knn" : request.json['AlBriqueta_knn'],
        "AlPosta_knn" : request.json['AlPosta_knn'],
        "FerroSi_gradoC_lf" : request.json['FerroSi_gradoC_lf'] ,
        "SiliMgStd_gradoD_lf" : request.json['SiliMgStd_gradoD_lf'], 
        "Alalambre_lf" : request.json['Alalambre_lf']
        }
    val = parameter["bool"]
    if val == "True":
        from main import codigo
        cd = codigo()
        return jsonify({"message":'Datos guardados exitosamente', "resultados":cd.algoritmo(parameter)})
    else:
        return jsonify({"message":'Se rechazo la solicitud'})
    return parameter
    
app.run(debug=True, port= 4000)
