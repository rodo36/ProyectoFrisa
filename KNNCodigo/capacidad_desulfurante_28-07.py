import pandas as pd
import numpy as np
import math
'''
Este codigo es para calcular la capacidad de sulfuración de la escoria.
'''

## Variables de entrada
Peso_liquido = 200000
Peso_escoria = 2845.2
Temperatura_C = 1600
Temperatura_K = 1873
Porcentaje_Ox_LF = 10
Porcentaje_S_LF = 0.009


Oxidos_entrada = {
    'CaO': 43,
    'SiO2': 22.9,
    'MgO': 16,
    'Al2O3': 2,
    'MnO': 14.1,
    'FeO': 1.8

}

Optical_basicity ={
    'CaO': 1,
    'SiO2' : 0.48,
    'MgO' : 0.78,
    'Al2O3' : 0.61,
    'MnO': 0.6,
    'FeO' : 0.6
}

Molecular_Weight ={
    'CaO' : 56.08,
    'SiO2' : 60.09,
    'MgO' : 40.31,
    'Al2O3' : 101.96,
    'MnO' : 70.94,
    'FeO' : 71.85
}

Num_oxigenos ={
    'CaO' : 1,
    'SiO2' : 2,
    'MgO' : 1,
    'Al2O3' : 3,
    'MnO' : 1,
    'FeO' : 1
}

R2O3 = Oxidos_entrada.get('MnO') + Oxidos_entrada.get('FeO')
limite_R2O3 = 20

#Acomodo de diccionarios en DataFrame
df = pd.concat( [pd.DataFrame(Oxidos_entrada.values()) ,pd.DataFrame(Optical_basicity.values()),
                 pd.DataFrame(Molecular_Weight.values()),pd.DataFrame(Num_oxigenos.values())],axis=1 )
df.columns=['Oxidos_entrada', 'Optical_Basicity', 'Molecular_Weight','Num_Oxigenos']
df.index=['CaO', 'SiO2', 'MgO', 'Al2O3', 'MnO', 'FeO']

##Calculos
#Oxidos de entrada / Peso molecular
df['Moles'] = df['Oxidos_entrada']/df['Molecular_Weight']

#suma Moles
suma_moles = sum(df['Moles'])

#Mole Fraccion (N)
df['Mole_Fraccion(N)']=df['Moles']/suma_moles

#Total Base Value
Total_base_value = sum(df['Mole_Fraccion(N)']*df['Num_Oxigenos'])

#X_values
df['X_values'] =df['Mole_Fraccion(N)'] /Total_base_value *df['Optical_Basicity'] *df['Num_Oxigenos']

#Optical Basicity
Optical_basicity_sum = sum(df['X_values'])

#Log_CS*
Log_CS1= ((22690 - (54640*Optical_basicity_sum)) / Temperatura_K) + (( 43.6* Optical_basicity_sum) - 25.2)

#Log CS
Log_CS0 = (12.6*Optical_basicity_sum) -12.3

#Ls
Ls0 = (10**Log_CS1) * (math.exp( (157863 - ((1600+273) * 56.8) ) / (1600+273) / 8.314 ) ) / (Porcentaje_Ox_LF/10000)

#Ls*
Ls1 = (10**Log_CS1)*(1/0.06) / (Porcentaje_Ox_LF / 10000)

#Ls**
Ls2 = 10**(Log_CS1- (math.log10(Porcentaje_Ox_LF/10000)) + (935/(1600+273)) + 1.375 )

#S final escoria
S_final_acero = (Porcentaje_S_LF * Peso_liquido) / (Peso_liquido + (Ls2 * Peso_escoria))