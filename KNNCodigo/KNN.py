import math
import pandas as pd
class KNN:  #parametros de KNN k_muestras = 6, d0 = 1
    def __init__(self):
        pass
    
    def distancia(self, dataVal, dataTrain):
        matrizdis = []
        for m in range(0, len(dataVal)):
            distancia = []
            for j in range(0, len(dataTrain)):
                suma = 0
                for k in range(0, dataTrain.shape[1]):
                    suma = suma + ((dataTrain[j][k] - dataVal[m][k]) ** 2)
                dis = int(1000 * math.sqrt(suma))
                distancia.append(dis)
            matrizdis.append(distancia)
        return matrizdis
    
    def ordenar(self, dataVal, dataTrain, matrizdis):
        indice = []
        for m in range(0, len(dataVal)):
            rowIndice = []
            for t in range(0, len(dataTrain)):
                rowIndice.append(t)
            indice.append(rowIndice)
        disOrdenada = matrizdis
        for m in range(0, len(dataVal)):
            for t in range(0, len(dataTrain) + 1):
                for j in range(0, len(dataTrain) - 1 - t):
                    if disOrdenada[m][j] > disOrdenada[m][j + 1]:
                        aux = disOrdenada[m][j]
                        disOrdenada[m][j] = disOrdenada[m][j + 1]
                        disOrdenada[m][j + 1] = aux
                        aux2 = indice[m][j]
                        indice[m][j] = indice[m][j + 1]
                        indice[m][j + 1] = aux2
        return (disOrdenada, indice)

    def promediOut(self, indice, salidas):
        k_muestras = 6
        muestras = []
        for i in range(0, len(indice)):
            muestra = 0
            for j in range(0, k_muestras):
                muestra = salidas.iloc[indice.iloc[i, j], :]
                muestras.append(muestra)
        muestras = pd.DataFrame(muestras)
        Prom = []
        for i in range(0, len(muestras), k_muestras):
            promedio = muestras.iloc[i:i + k_muestras, :].mean(axis=0)
            Prom.append(promedio)
        Prom = pd.DataFrame(Prom)
        return Prom

    def asignacion(self, dataVal, salidas_train, salidas_norm_train, disOrdenada):#Esta funcion no se usa en main pero la conservo si se llega a necesitar en un futuro
        SalidaEscoriaKNN = []
        matsalida = []
        K_muestras = 6
        d0 = 1
        ##Salida- Respuesta- Asignacion de variable
        for m in range(0, len(dataVal)):
            rowSalidaEscoriaKNN = []
            for i in range(0, salidas_train.shape[1]):
                Denominador = 0
                Numerador = 0
                for j in range(0, K_muestras):
                    Numerador = float(Numerador) + (float(salidas_norm_train.values[indice[m][j]][i])) / (
                            float(disOrdenada[m][j]) + d0)
                    Denominador = float(Denominador) + (float(1) / (float(disOrdenada[m][j]) + d0))
                    a = Numerador
                    B = Denominador
                rowSalidaEscoriaKNN.append(float(Numerador) / float(Denominador))
            SalidaEscoriaKNN.append(rowSalidaEscoriaKNN)
        return SalidaEscoriaKNN