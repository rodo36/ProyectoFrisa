import pandas as pd
import numpy as np
import math
import easygui
from pandas import ExcelWriter
import matplotlib.pyplot as plt
import seaborn as sns

# cargamos los datos de entrada
xls = pd.ExcelFile(r'C:/Users/Econ/Documents/A Ma Luisa/Proyecto frisa/Frisa 2/Data_train_val_frisa3.xlsx')
# Data train
df_train1 = pd.read_excel(xls, 'DataTrain')
data_train = df_train1.iloc[:,0:14]
#Data val
df_val = pd.read_excel(xls, 'DataVal')
data_val = df_val.iloc[:,0:14]
#Salidas train
df_salidas_train = pd.read_excel(xls, 'SalidasTrain')
salidas_train = df_salidas_train
#Salidas val
df_salidas_val= pd.read_excel(xls, 'SalidasVal')
salidas_val = df_salidas_val


## Parametros y data_validacion_train and Salidas_validacion_train
K_muestras = 6
d0 = 1
data_train_Val = pd.concat([data_train, data_val], axis=0)
data_salidas_trainval = pd.concat([salidas_train, salidas_val], axis=0)

##_______Normalizamos los datos de entrada___________
# Funcion de normalizacion
def normalize(dataframe):
    result = dataframe.copy()
    for feature_name in dataframe.columns:
        max_value = dataframe[feature_name].max()
        min_value = dataframe[feature_name].min()
        result[feature_name] = (dataframe[feature_name] - min_value) / (max_value - min_value)
    return result

data_train_Val_norm = normalize(data_train_Val)
data_salidas_trainval_norm = normalize(data_salidas_trainval)

##Separacion de data normalizada

# Data train normalizado
data_norm_train = data_train_Val_norm.iloc[0:len(data_train), :]
# Data Val normalizado
data_norm_val = data_train_Val_norm.iloc[len(data_train):len(data_train_Val_norm), :]

# Salidas train normalizado
salidas_norm_train = data_salidas_trainval_norm.iloc[0:len(data_train), :]
# Salidas val normalizado
salidas_norm_val = data_salidas_trainval_norm.iloc[len(data_train):len(data_train_Val_norm), :]

###KNN
rows1 = data_norm_val.values  # data val
rowsKNN1 = data_norm_train.values  # data train
# dfP2O5HF=dfP2O5HF1.values

##Distancias
matDistancia = []
for m in range(0, len(rows1)):
    distancia = []
    for j in range(0, len(rowsKNN1)):
        suma = 0
        for k in range(0, rowsKNN1.shape[1]):
            suma = suma + ((rowsKNN1[j][k] - rows1[m][k]) ** 2)
        dis = int(1000 * math.sqrt(suma))
        distancia.append(dis)
    matDistancia.append(distancia)  # sale de una dimension len(rows1) x len(rowsKNN1)

##Indices y matriz ordenada
Indice = []
for m in range(0, len(rows1)):
    rowIndice = []
    for t in range(0, len(rowsKNN1)):
        rowIndice.append(t)
    Indice.append(rowIndice)

Distanciaordenada = matDistancia

for m in range(0, len(rows1)):
    for t in range(0, len(rowsKNN1) + 1):
        for j in range(0, len(rowsKNN1) - 1 - t):
            if Distanciaordenada[m][j] > Distanciaordenada[m][j + 1]:
                aux = Distanciaordenada[m][j]
                Distanciaordenada[m][j] = Distanciaordenada[m][j + 1]
                Distanciaordenada[m][j + 1] = aux

                aux2 = Indice[m][j]
                Indice[m][j] = Indice[m][j + 1]
                Indice[m][j + 1] = aux2



##Salida- Respuesta- Asignacion de variable
SalidaEscoriaKNN = []
matsalida = []
for m in range(0, len(rows1)):
    rowSalidaEscoriaKNN = []
    for i in range(0, salidas_train.shape[1]):
        Denominador = 0
        Numerador = 0
        for j in range(0, K_muestras):
            Numerador = float(Numerador) + (float(salidas_norm_train.values[Indice[m][j]][i])) / (
                    float(Distanciaordenada[m][j]) + d0)
            Denominador = float(Denominador) + (float(1) / (float(Distanciaordenada[m][j]) + d0))
            a = Numerador
            B = Denominador

        rowSalidaEscoriaKNN.append(float(Numerador) / float(Denominador))
    SalidaEscoriaKNN.append(rowSalidaEscoriaKNN)

##Funcion promedio como salida
Indice_df = pd.DataFrame(Indice)
muestras = []
for i in range(0, len(Indice_df)):
    muestra = 0
    for j in range(0, K_muestras):
        muestra = salidas_norm_train.iloc[Indice_df.iloc[i, j], :]
        muestras.append(muestra)
muestras = pd.DataFrame(muestras)

Prom = []
for i in range(0, len(muestras), K_muestras):
    promedio = muestras.iloc[i:i + K_muestras, :].mean(axis=0)
    Prom.append(promedio)
Prom = pd.DataFrame(Prom)  # aqui estan mis salidas

##Cuales son las muestras que se parecen a mi muestra a clasificar

Indice_k = pd.DataFrame(Indice).iloc[:, 0:K_muestras]

'''muestras= []
data_salidas_train = pd.concat([data_train, salidas_train],axis=1)
for i in range(0, len(data_norm_val)): 
    for j in range(0, K_muestras):  
        muestra = data_salidas_train.iloc[Indice_k.iloc[i,j],:]
        muestras.append(muestra)
Muestras_7 = pd.DataFrame(muestras)
maria = pd.DataFrame(Matrizordenada).iloc[:,0:7].mean(axis=1)
'''
##Desnormalizar los datos
i = 0
j = 0
res_calDol = []
res_calSiderGruesa = []
res_calSiderFina = []
res_AluminioBriqueta = []
res_AluminioPosta = []
for i in range(0, len(Prom)):
    df2 = Prom.values  # SalidaEscoriaKNN  # dataframe que quiero desnormalizar
    res1 = ((data_salidas_trainval['CalDol_LF'].max() - data_salidas_trainval['CalDol_LF'].min()) *
            df2[i][0]) + data_salidas_trainval['CalDol_LF'].min()
    res_calDol.append(res1)
    res2 = ((data_salidas_trainval['CalSiderGruesa_LF'].max() - data_salidas_trainval['CalSiderGruesa_LF'].min()) *
            df2[i][1]) + data_salidas_trainval['CalSiderGruesa_LF'].min()
    res_calSiderGruesa.append(res2)
    res22 = ((data_salidas_trainval['CalSiderFina_LF'].max() - data_salidas_trainval['CalSiderFina_LF'].min()) *
             df2[i][2]) + data_salidas_trainval['CalSiderFina_LF'].min()
    res_calSiderFina.append(res22)
    res3 = ((data_salidas_trainval['AlBriqueta_LF'].max() - data_salidas_trainval[
        'AlBriqueta_LF'].min()) * df2[i][3]) + data_salidas_trainval['AlBriqueta_LF'].min()
    res_AluminioBriqueta.append(res3)
    res33 = ((data_salidas_trainval['AlPosta_LF'].max() - data_salidas_trainval[
        'AlPosta_LF'].min()) * df2[i][4]) + data_salidas_trainval['AlPosta_LF'].min()
    res_AluminioPosta.append(res33)

##Resultados
resultados2 = pd.concat([pd.DataFrame(res_calDol), pd.DataFrame(res_calSiderGruesa), pd.DataFrame(res_calSiderFina),
                         pd.DataFrame(res_AluminioBriqueta), pd.DataFrame(res_AluminioPosta)],
                        axis=1)  # son mis salidas del algoritmo KNN
resultados2.columns = ['Cal Dolomitica LF', 'Cal Sider Gruesa LF', 'Cal Sider Fina LF', 'Aluminio Briqueta LF',
                       'Aluminio Posta Lf']
# resultados2.to_excel('ResultadosFrisa_Promedio_100NN.xlsx', engine='xlsxwriter')

##Parte quimica
lista = []
Al203total = []
B4Atotal = []
CaOtotal = []

data_quimica1 = data_val.iloc[:,0:10]

data_quimica1['Al_PostaBriqueta_TAP'] = data_quimica1.iloc[:,7] + data_quimica1.iloc[:,6]
#data_quimica['Al_PostaBriqueta_LF'] = data_quimica.iloc[:, 14] + data_quimica.iloc[:, 15]

data_quimica = pd.concat([data_quimica1, df_val.iloc[:,17:21]], axis=1)
data_quimica_colnames = pd.DataFrame(list(data_quimica.columns))
rows = np.array(data_quimica)

##LEctura de parametros para la estimacion quimica

Parametro_Ox_Al1 = 30
AlPostas_Quimica_Al = 97

# Constante de ajuste para la masa de oxidos
Cte_Al2O3 = 43.17 #76.659
Cte_SiO2 = 0 #3.154
Cte_CaO =  0 #42.83 #mas baja
Cte_MnO = 0
Cte_MgO = 0

# Lectura de la riqueza de la ferroaleacion
# Aluminio Postas/Al Briquetas
AlBrique_Quimica_Al = 97
# AlPostas_Quimica_Al = 97 lo puse arriba de data quimica
Al_brique_Quimica_Si = 0.1
Al_Postas_Quimica_Si = 0.1

# Aluminio en alambre para ajuste de la composicion quimica del acero
# Quimica de las adiciones
AlAlambre_AL = 100

# Cal Dolomitica Gruesa

CalDolomitica_QuimicaCaO = 40 #50
CalDolomitica_QuimicaMgO = 30
CalDolomitica_QuimicaMnO = 0
CalDolomitica_QuimicaAl2O3 = 1
CalDolomitica_QuimicaP2O5 = 0
CalDolomitica_QuimicaSiO = 2.5
CalDolomitica_QuimicaFeO = 0

# Cal Siderurgica fina
CalSiderurgica_fina_QuimicaCaO = 70# 90
CalSiderurgica_fina_QuimicaMgO = 2#2.5
CalSiderurgica_fina_QuimicaMnO = 0
CalSiderurgica_fina_QuimicaAl2O3 = 5
CalSiderurgica_fina_QuimicaAl = 0
CalSiderurgica_fina_QuimicaP2O5 = 0
CalSiderurgica_fina_QuimicaSiO = 2.5
CalSiderurgica_fina_QuimicaFe2O3 = 0.5
CalSiderurgica_fina_QuimicaFeO = 0

# Cal Siderurgica Gruesa
CalSiderurgica_Gruesa_QuimicaCaO = 70#90
CalSiderurgica_Gruesa_QuimicaMgO = 2#2.5
CalSiderurgica_Gruesa_QuimicaMnO = 0
CalSiderurgica_Gruesa_QuimicaAl2O3 = 0.5
CalSiderurgica_Gruesa_QuimicaP2O5 = 0
CalSiderurgica_Gruesa_QuimicaSiO = 2.5
CalSiderurgica_Gruesa_QuimicaFe2O3 = 0.5
CalSiderurgica_Gruesa_QuimicaAl = 0
CalSiderurgica_Gruesa_QuimicaFeO = 0

# FeSi Grado C

FeSi_GC_QuimicaCaO = 0
FeSi_GC_QuimicaMgO = 0
FeSi_GC_QuimicaMnO = 0
FeSi_GC_QuimicaAl2O3 = 0.5  # = FeSi_GC_QuimicaAl , es el contenido de Al
FeSi_GC_QuimicaP2O5 = 0
FeSi_GC_QuimicaSi = 74.3
FeSi_GC_QuimicaFeO = 0
FeSi_GC_QuimicaMn = 0
FeSi_GC_QuimicaP = 0.019 #0.019

# Silico Manganeso Estandar

SiMnSTD_QuimicaCaO = 0
SiMnSTD_QuimicaMgO = 0
SiMnSTD_QuimicaMn = 66
SiMnSTD_QuimicaAl2O3 = 0
SiMnSTD_QuimicaP2O5 = 0
SiMnSTD_QuimicaSiO = 0
SiMnSTD_QuimicaFeO = 0
SiMnSTD_QuimicaSi = 16.1
SiMnSTD_QuimicaP = 0.05  #0.16

'''# FeSi
FeSi_QuimicaCaO = 0
FeSi_QuimicaMgO = 0
FeSi_QuimicaMnO = 0
FeSi_QuimicaAl2O3 = 0
FeSi_QuimicaP2O5 = 0
FeSi_QuimicaSiO = 0
FeSi_QuimicaFeO = 0
FeSi_QuimicaSi = 77'''

# Leer Rendimientos de Ferroaleaciones

Rend_SI_SiMnSTD = 65
Rend_Mn_SiMnSTD = 97 #91
Rend_FeSi_GC = 75#70

# Leer Factores de conversion
Si_to_SiO2 = 2.14
Mn_to_MnO = 1.29
Al_to_Al2O3 = 1.89
FeT_to_FeO = 1.29
P_to_P2O5 = 2.29

# Lectura de aportes adicionales
# Composicion quimica de la escoria EAF % tipico de distribucion de los oxidos (debe ser ajustado en base a la estadistica de frisa)

# CaO
CaO_porc_statHF = 0.48
# MgO
MgO_porc_statHF = 0.12
# Al2O3
Al2O3_porc_statHF = 0.31
# SiO2
SiO2_porc_statHF = 0.003
# MnO
MnO_porc_statHF = 0.04
# FeO
FeO_porc_statHF = 0.4

# P2O5

P2O5_porc_statHF = 0.3

# Lectura de quimica tipica en porcentaje y kg de pase de arena fijos


# CaO
CaO_porc_EBT = 0
# MgO
MgO_porc_EBT = 0.47
# Al2O3
Al2O3_porc_EBT = 0
# SiO2
SiO2_porc_EBT = 0.405
# MnO
MnO_porc_EBT = 0
# FeO
FeO_porc_EBT = 0
# LECTURA Kilogramos de pase de arena de EBT

PaseArena_EBT_teorico = 50

##Empieza for
listalistas = []
maria = []
maria2 = []
Escoria_TotalWeight = 0.0
index = 0
row = []
for i in range(0, len(rows)):
    Al203 = []
    B4A = []
    CaO = []
    Al203.append(index)
    B4A.append(index)
    CaO.append(index)
    index += 1

    Ton_vaciado = rows[i][1]
    # adiciones tap
    CalDolomitica_tap = rows[i][3]
    CalSiderurgica_gruesa_tap = rows[i][4]
    CalSiderurgica_fina_tap = rows[i][5]
    AlBriqueta_tap = rows[i][6]
    AlPosta_tap = rows[i][7]
    FerroSi_gradoC_tap = rows[i][8]
    SiMnStd_gradoD_tap = rows[i][9]
    AlPostaBriquetas_tap = rows[i][10]  # =suma de cols 9+10
    # adiciones lf
    FerroSi_gradoC_lf = rows[i][11]
    SiliMgStd_gradoD_lf = rows[i][12]
    Alalambre_lf = rows[i][13]
    '''
    CalSiderurgica_gruesa_lf = rows[i][9]
    CalDolomitica_gruesa_lf = rows[i][10]
    CalSiderurgica_fina_lf = rows[i][11]
    AlPostas_lf = rows[i][14]
    AlBriquetas_lf = rows[i][15]

    # variables composicion quimica de la escoria EAF
    CaO_EAF = rows[i][16]
    Al2O3_EAF = rows[i][17]
    FeO_EAF = rows[i][18]
    MgO_EAF = rows[i][19]
    MnO_EAF = rows[i][20]
    P2O5_EAF = rows[i][21]
    SiO2_EAF = rows[i][22]

    # Declaracion de variables de composicion quimica de la escoria LF inicial
    CaO_Lfinicial = rows[i][23]
    Al2O3_Lfinicial = rows[i][24]
    FeO_Lfinicial = rows[i][25]
    MgO_Lfinicial = rows[i][26]
    MnO_Lfinicial = rows[i][27]
    P2O5_Lfinicial = rows[i][28]
    SiO2_Lfinicial = rows[i][29]

    Mn_lf2 = rows[i][31]
    Mn_lf1 = rows[i][30]
    Si_lf2 = rows[i][33]
    Si_lf1 = rows[i][32]
    Al_lf2 = rows[i][35]
    Al_lf1 = rows[i][34]'''
    Parametro_Ox_Al = 30  # rows[i][38] #capacidad del al que al añadirlo , se oxida

    # Calculo de masas de oxidos y fluoruros en la escoria

    ##Oxidos producidos por adiciones en tapping
    # Calculo de masa de alumina producida por adiciones en tapping
    Mass_Al2O3_total_tap = (Cte_Al2O3 + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
            CalDolomitica_tap * CalDolomitica_QuimicaAl2O3 / 100) +
                            (CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaAl2O3 / 100) + (
                                    (FerroSi_gradoC_tap * FeSi_GC_QuimicaAl2O3 * (100-Rend_FeSi_GC) / 10000) * Al_to_Al2O3) +
                            (AlPosta_tap * (100 - Parametro_Ox_Al) * AlPostas_Quimica_Al / 10000 * Al_to_Al2O3) + (
                                    AlBriqueta_tap * (
                                    100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3))

    # CaO producidas por adiciones de tapping
    Mass_CaO_total_tap = (Cte_CaO + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaCaO / 100) + (
            CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaCaO / 100) +
                          (CalDolomitica_tap * CalDolomitica_QuimicaCaO / 100))
    # Si producido por adiciones de tapping
    Mass_SiO2_total_tap = (Cte_SiO2 + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaSiO / 100) + (
            CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaSiO / 100) +
                           (CalDolomitica_tap * CalDolomitica_QuimicaSiO / 100) + (((
                    FerroSi_gradoC_tap * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                           ((SiMnStd_gradoD_tap * SiMnSTD_QuimicaSi * (100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) + (
                                   AlPosta_tap * (
                                   100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2) +
                           (AlBriqueta_tap * (100 - Parametro_Ox_Al) * Al_brique_Quimica_Si / 10000 * Si_to_SiO2))
    # MnO producido por adiciones de tapping
    Mass_MnO_total_tap = (
            Cte_MnO + (SiMnStd_gradoD_tap * (100 - Rend_Mn_SiMnSTD) * SiMnSTD_QuimicaMn / 10000 * Mn_to_MnO))
    # MgO producido por adiciones de tapping
    Mass_MgO_total_tap = (Cte_MgO + (CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaMgO / 100) + (
            CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaMgO / 100) +
                          (CalDolomitica_tap * CalDolomitica_QuimicaMgO / 100))
    # P2O5 producido por adiuciones en tapping
    Mass_P2O5_total_tap = ((FerroSi_gradoC_tap * FeSi_GC_QuimicaP * (100- Rend_FeSi_GC) / 10000 * P_to_P2O5) + (
            SiMnStd_gradoD_tap * SiMnSTD_QuimicaP * (100- Rend_SI_SiMnSTD) / 10000 * P_to_P2O5))

    Mass_Fe2O3_total_tap = ((CalSiderurgica_gruesa_tap * CalSiderurgica_Gruesa_QuimicaFe2O3 / 100) + (
            CalSiderurgica_fina_tap * CalSiderurgica_fina_QuimicaFe2O3 / 100))

    row.append(Mass_Al2O3_total_tap)
    row.append(Mass_CaO_total_tap)
    row.append(Mass_SiO2_total_tap)
    row.append(Mass_MnO_total_tap)
    row.append(Mass_MgO_total_tap)
    row.append(Mass_P2O5_total_tap)
    row.append(Mass_Fe2O3_total_tap)
    ##Escoria total
    Escoria_total_tap = Mass_Al2O3_total_tap + Mass_CaO_total_tap + Mass_SiO2_total_tap + Mass_MnO_total_tap + Mass_MgO_total_tap + Mass_P2O5_total_tap + Mass_Fe2O3_total_tap
    row.append(Escoria_total_tap)
    ##Porcentajes de oxidos primera muestra lf1
    Porc_Al2O3_tap = (Mass_Al2O3_total_tap * 100 / Escoria_total_tap)
    Porc_CaO_tap = (Mass_CaO_total_tap * 100 / Escoria_total_tap)
    Porc_SiO_tap = (Mass_SiO2_total_tap * 100 / Escoria_total_tap)
    Porc_MnO_tap = (Mass_MnO_total_tap * 100 / Escoria_total_tap)
    Porc_MgO_tap = (Mass_MgO_total_tap * 100 / Escoria_total_tap)
    Porc_P2O5_tap = (Mass_P2O5_total_tap * 100 / Escoria_total_tap)
    Porc_Fe2O3_tap = (Mass_Fe2O3_total_tap * 100 / Escoria_total_tap)
    BA3_tap = Porc_CaO_tap / (Porc_SiO_tap + Porc_Al2O3_tap)
    Relacion_CaOAl2O3_Tap = Porc_CaO_tap / Porc_Al2O3_tap
    row.append(Porc_Al2O3_tap)
    row.append(Porc_CaO_tap)
    row.append(Porc_SiO_tap)
    row.append(Porc_MnO_tap)
    row.append(Porc_MgO_tap)
    row.append(Porc_P2O5_tap)
    row.append(Porc_Fe2O3_tap)
    row.append(BA3_tap)
    row.append(Relacion_CaOAl2O3_Tap)

    ## Lectura de salidas del KNN
    CalDol_knn = resultados2.iloc[:, 0].values[i]
    CalSiderGruesa_knn = resultados2.iloc[:, 1].values[i]
    CalSiderFina_knn = resultados2.iloc[:, 2].values[i]
    AlBriqueta_knn = resultados2.iloc[:, 3].values[i]
    AlPosta_knn = resultados2.iloc[:, 4].values[i]
    row.append(CalDol_knn)
    row.append(CalSiderGruesa_knn)
    row.append(CalSiderFina_knn)
    row.append(AlBriqueta_knn)
    row.append(AlPosta_knn)
    maria.append(CalDol_knn)
    maria.append(CalSiderGruesa_knn)
    maria.append(CalSiderFina_knn)
    maria.append(AlBriqueta_knn)
    maria.append(AlPosta_knn)
    ## Balance posterior a la adicion
    # Calculo de masa de alumina producida por adiciones en lf
    ##Oxidos producidos por adicionse de lf2 =  ULF
    Mass_Al2O3_total_lf2 = (Cte_Al2O3 + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
            CalDol_knn * CalDolomitica_QuimicaAl2O3 / 100) +
                            (CalSiderFina_knn * CalSiderurgica_fina_QuimicaAl2O3 / 100) + (
                                    (FerroSi_gradoC_lf * FeSi_GC_QuimicaAl2O3 * (100-Rend_FeSi_GC)  / 10000) * Al_to_Al2O3) +
                            (AlPosta_knn * (100 - Parametro_Ox_Al) * AlPostas_Quimica_Al / 10000 * Al_to_Al2O3) + (
                                    AlBriqueta_knn * (
                                    100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3)
                            + (Alalambre_lf * (100 - Parametro_Ox_Al) * 100 / 10000 * Al_to_Al2O3)
                            + Mass_Al2O3_total_tap)

    Mass_CaO_total_lf2 = (Cte_CaO + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaCaO / 100) + (
            CalSiderFina_knn * CalSiderurgica_fina_QuimicaCaO / 100) +
                          (CalDol_knn * CalDolomitica_QuimicaCaO / 100) + Mass_CaO_total_tap)

    Mass_SiO2_total_lf2 = (Cte_SiO2 + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaSiO / 100) + (
            CalSiderFina_knn * CalSiderurgica_fina_QuimicaSiO / 100) +
                           (CalDol_knn * CalDolomitica_QuimicaSiO / 100) + (((
                    FerroSi_gradoC_lf * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                           ((SiliMgStd_gradoD_lf * SiMnSTD_QuimicaSi * (
                                   100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) + (
                                   AlPosta_knn * (
                                   100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2) +
                           (AlBriqueta_knn * (100 - Parametro_Ox_Al) * Al_brique_Quimica_Si / 10000 * Si_to_SiO2) +
                           Mass_SiO2_total_tap)

    Mass_MnO_total_lf2 = (
            Cte_MnO + (SiliMgStd_gradoD_lf * (100 - Rend_Mn_SiMnSTD) * SiMnSTD_QuimicaMn / 10000 * Mn_to_MnO) +
            Mass_MnO_total_tap)

    Mass_MgO_total_lf2 = (Cte_MgO + (CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaMgO / 100) + (
            CalSiderFina_knn * CalSiderurgica_fina_QuimicaMgO / 100) +
                          (CalDol_knn * CalDolomitica_QuimicaMgO / 100) + Mass_MgO_total_tap)

    Mass_P2O5_total_lf2 = ((FerroSi_gradoC_lf * FeSi_GC_QuimicaP * (100-Rend_FeSi_GC) / 10000 * P_to_P2O5) + (
            SiliMgStd_gradoD_lf * SiMnSTD_QuimicaP * (100-Rend_SI_SiMnSTD) / 10000 * P_to_P2O5) + Mass_P2O5_total_tap)

    Mass_Fe2O3_total_lf2 = ((CalSiderFina_knn * CalSiderurgica_fina_QuimicaFe2O3 / 100) + (
            CalSiderGruesa_knn * CalSiderurgica_Gruesa_QuimicaFe2O3 / 100) + Mass_Fe2O3_total_tap)

    row.append(Mass_Al2O3_total_lf2)
    row.append(Mass_CaO_total_lf2)
    row.append(Mass_SiO2_total_lf2)
    row.append(Mass_MnO_total_lf2)
    row.append(Mass_MgO_total_lf2)
    row.append(Mass_P2O5_total_lf2)
    row.append(Mass_Fe2O3_total_lf2)
    ##Escoria total
    Escoria_total_lf2 = Mass_Al2O3_total_lf2 + Mass_CaO_total_lf2 + Mass_SiO2_total_lf2 + Mass_MnO_total_lf2 + Mass_MgO_total_lf2 + Mass_P2O5_total_lf2 + Mass_Fe2O3_total_lf2
    row.append(Escoria_total_lf2)

    ##Porcentajes de oxidos primera muestra lf
    Porc_Al2O3_lf2 = ((Mass_Al2O3_total_lf2) * 100 / Escoria_total_lf2)
    Porc_CaO_lf2 = ((Mass_CaO_total_lf2) * 100 / Escoria_total_lf2)
    Porc_SiO_lf2 = ((Mass_SiO2_total_lf2) * 100 / Escoria_total_lf2)
    Porc_MnO_lf2 = ((Mass_MnO_total_lf2) * 100 / Escoria_total_lf2)
    Porc_MgO_lf2 = ((Mass_MgO_total_lf2) * 100 / Escoria_total_lf2)
    Porc_P2O5_lf2 = ((Mass_P2O5_total_lf2) * 100 / Escoria_total_lf2)
    Porc_Fe2O3_lf2 = ((Mass_Fe2O3_total_lf2 * 100) / Escoria_total_lf2)

    ##Recalculo de MnO
    Cte_MnO_Corr1 = 0.07212
    Cte_MnO_Corr2 = 0.03344
    Porc_MnO_corregido = Porc_MnO_lf2 * Cte_MnO_Corr1 + Cte_MnO_Corr2
    Mass_MnO_lf2_corregida = Porc_MnO_corregido * Escoria_total_lf2 / 100
    Mass_MnO_corregida_new = Mass_MnO_total_lf2 - Mass_MnO_lf2_corregida
    Mass_Escoria_lf2_new = Mass_Al2O3_total_lf2 + Mass_CaO_total_lf2 + Mass_SiO2_total_lf2 + Mass_MnO_corregida_new + Mass_MgO_total_lf2 + Mass_P2O5_total_lf2 + Mass_Fe2O3_total_lf2

    # Porcentajes de oxidos corregidos por MnO
    Porc_Al2O3_lf2_new = ((Mass_Al2O3_total_lf2) * 100 / Mass_Escoria_lf2_new)
    Porc_CaO_lf2_new = ((Mass_CaO_total_lf2) * 100 / Mass_Escoria_lf2_new)
    Porc_SiO_lf2_new = ((Mass_SiO2_total_lf2) * 100 / Mass_Escoria_lf2_new)
    Porc_MnO_lf2_new = ((Mass_MnO_total_lf2) * 100 / Mass_Escoria_lf2_new)
    Porc_MgO_lf2_new = ((Mass_MgO_total_lf2) * 100 / Mass_Escoria_lf2_new)
    Porc_P2O5_lf2_new = ((Mass_P2O5_total_lf2) * 100 / Mass_Escoria_lf2_new)
    Porc_Fe2O3_lf2_new = ((Mass_Fe2O3_total_lf2 * 100) / Mass_Escoria_lf2_new)
    BA3_lf2_new = Porc_CaO_lf2_new / (Porc_SiO_lf2_new + Porc_Al2O3_lf2_new )
    Relacion_CaOAl2O3_lf2_new = Porc_CaO_lf2_new /Porc_Al2O3_lf2_new

    row.append(Porc_Al2O3_lf2_new)  # 16.80
    row.append(Porc_CaO_lf2_new)  # 54.37
    row.append(Porc_SiO_lf2_new)
    row.append(Porc_MnO_lf2_new)
    row.append(Porc_MgO_lf2_new)
    row.append(Porc_P2O5_lf2_new)
    row.append(Porc_Fe2O3_lf2_new)
    row.append(BA3_lf2_new)
    row.append(Relacion_CaOAl2O3_lf2_new )

    ##Optmizacion
    # f = float(Escoria_TotalFeO)
    Escoria_TotalMassAl2O3 = Mass_Al2O3_total_lf2
    Escoria_TargetAl2O3 = 30
    Escoria_TotalCaO = Porc_CaO_lf2 #_new
    Escoria_TotalMgO = Porc_MgO_lf2#_new
    Escoria_TotalSiO2 = Porc_SiO_lf2#_new
    Escoria_TotalAl2O3 = Porc_Al2O3_lf2#_new
    Escoria_Total_Fe2O3 = Porc_Fe2O3_lf2#_new
    Escoria_AlgranallaAddition = AlPosta_knn + AlBriqueta_knn
    Escoria_WeightAddition = Escoria_total_lf2  # Peso total de escoria
    Mass_Al2O3_Adicionales = Mass_Al2O3_total_lf2
    CalSiderVaciado_MassAl2O3 = CalSiderurgica_Gruesa_QuimicaAl2O3 + CalSiderurgica_fina_QuimicaAl2O3
    Escoria_TotalMassMgO = Mass_MgO_total_lf2
    Escoria_TotalMassCaO = Mass_CaO_total_lf2
    Escoria_TotalMassAl2O3 = Mass_Al2O3_total_lf2
    Escoria_TotalMassSiO2 = Mass_SiO2_total_lf2
    Escoria_TotalMassMnO = Mass_MnO_total_lf2
    Escoria_TotalMassFe2O3 = Mass_Fe2O3_total_lf2
    Escoria_TargetAl2O3 = 18# 19 #20
    Escoria_TargetBA4 = 2  # 1.8
    Escoria_TargetCaO = 56 # 58# 54
    CalSiderurgica_QuimicaCaO = CalSiderurgica_Gruesa_QuimicaCaO
    Escoria_LimeAddition = CalSiderGruesa_knn + CalSiderFina_knn

    # basicidad final
    Escoria_BA4 = (Escoria_TotalCaO + Escoria_TotalMgO) / (Escoria_TotalSiO2 + Escoria_TotalAl2O3)
    Escoria_Al2O3Tolerance = 0.1  # 0.1
    Escoria_AlGranallaStep = 0.1
    Escoria_DeltaAl2O3 = 2 * float(Escoria_Al2O3Tolerance)  # 0.2
    MAX_ITER = 10000
    Iter = 0
    Iter2 = 0
    # Escoria_AlgranallaAddition = Mass_Al2O3_Al_ALAM01 + Mass_AlPosta
    while ((abs(Escoria_DeltaAl2O3) > Escoria_Al2O3Tolerance) and (Escoria_AlgranallaAddition > 0) and (
            Escoria_LimeAddition > 0) and Iter <= MAX_ITER):
        Iter = Iter + 1
        while ((abs(Escoria_DeltaAl2O3) > Escoria_Al2O3Tolerance) and (Escoria_AlgranallaAddition > 0) and (
                Escoria_LimeAddition > 0) and Iter2 <= MAX_ITER):
            Iter2 = Iter2 + 1
            Escoria_AlgranallaAddition = Escoria_AlgranallaAddition - Escoria_AlGranallaStep

            if (Escoria_DeltaAl2O3 >= Escoria_Al2O3Tolerance):
                Escoria_AlgranallaAddition = Escoria_AlgranallaAddition - Escoria_AlGranallaStep

                if (Escoria_AlgranallaAddition < 0):
                    Escoria_AlgranallaAddition = 0

                Escoria_TotalWeight = float(Escoria_TotalWeight) - float(Escoria_AlGranallaStep)


            else:
                if (Escoria_DeltaAl2O3 <= ((-1) * Escoria_Al2O3Tolerance)):
                    Escoria_AlgranallaAddition = Escoria_AlgranallaAddition + Escoria_AlGranallaStep
                    Escoria_TotalWeight = Escoria_WeightAddition + Escoria_AlGranallaStep

            # Total weights of additions
            AlGranalla_MassAl2O3 = (Cte_Al2O3 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
                    CalDol_knn * CalDolomitica_QuimicaAl2O3 / 100) +
                                    + ((FerroSi_gradoC_lf * FeSi_GC_QuimicaAl2O3 * (100- Rend_FeSi_GC ) / 10000) * Al_to_Al2O3) +
                                    + (Escoria_AlgranallaAddition * (
                                            100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3)
                                    + (Alalambre_lf * (100 - Parametro_Ox_Al) * 100 / 10000 * Al_to_Al2O3)
                                    )
            AlGranalla_MassSiO2 = (Cte_SiO2 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaSiO / 100) +
                                   (CalDol_knn * CalDolomitica_QuimicaSiO / 100) + (((
                            FerroSi_gradoC_lf * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                                   ((SiliMgStd_gradoD_lf * SiMnSTD_QuimicaSi * (
                                           100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) +
                                   (Escoria_AlgranallaAddition * (
                                           100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2))

            Escoria_TotalMassAl2O3 = Mass_Al2O3_total_tap + AlGranalla_MassAl2O3

            Escoria_TotalMassSiO2 = Mass_SiO2_total_tap + AlGranalla_MassSiO2

            Escoria_WeightAddition = (
                    Escoria_TotalMassAl2O3 + Escoria_TotalMassSiO2 + Mass_CaO_total_lf2 + Mass_MnO_total_lf2 + Mass_MgO_total_lf2 + Mass_Fe2O3_total_lf2 + Mass_P2O5_total_lf2)

            # Final estimated slag composition with additions
            Escoria_TotalMgO = float(100) * float(Escoria_TotalMassMgO) / float(Escoria_WeightAddition)
            Escoria_TotalCaO = float(100) * float(Escoria_TotalMassCaO) / float(Escoria_WeightAddition)
            Escoria_TotalAl2O3 = float(100) * float(Escoria_TotalMassAl2O3) / float(Escoria_WeightAddition)
            C = float(Escoria_TotalAl2O3)
            Escoria_TotalSiO2 = float(100) * float(Escoria_TotalMassSiO2) / float(Escoria_WeightAddition)
            Escoria_TotalMnO = float(100) * float(Escoria_TotalMassMnO) / float(Escoria_WeightAddition)
            Escoria_Total_Fe2O3 = 100 * Escoria_TotalMassFe2O3 / Escoria_WeightAddition
            Escoria_Total_P2O5 = 100 * Mass_P2O5_total_lf2 / Escoria_WeightAddition

            # Basicity:
            Escoria_BA4 = (float(Escoria_TotalCaO) + float(Escoria_TotalMgO)) / (
                    float(Escoria_TotalSiO2) + float(Escoria_TotalAl2O3))
            Escoria_DeltaAl2O3 = float(Escoria_TotalAl2O3) - float(Escoria_TargetAl2O3)
            a = 1

        a = float(Escoria_AlgranallaAddition)
        B = float(Escoria_LimeAddition)
        C = float(a)

        # CaO that meets target BA4
        Escoria_CaoContent =  Escoria_TargetCaO #float(Escoria_TargetBA4) * (float(Escoria_TotalSiO2) + float(Escoria_TotalAl2O3)) #float(Escoria_BA4) * (float(Escoria_TotalSiO2) + float(Escoria_TargetAl2O3)) #

        Escoria_DeltaCaO = float(Escoria_CaoContent) - float(Escoria_TotalCaO)

        Escoria_MassCaOAddition = float(Escoria_TotalWeight) * (float(Escoria_DeltaCaO) / float(100))
        Escoria_FinalLimeAddition = float(Escoria_MassCaOAddition) * float(100) / float(CalSiderurgica_QuimicaCaO)
        Escoria_LimeAddition = float(Escoria_LimeAddition) + float(Escoria_FinalLimeAddition)
        Escoria_TotalWeight = float(Escoria_WeightAddition) + float(Escoria_FinalLimeAddition)
        Escoria_TotalMassCaO = float(Escoria_TotalMassCaO) + float(Escoria_MassCaOAddition)
        # Si Lime addition <0 -> lime addition =0
        if Escoria_LimeAddition < 0:
            Escoria_LimeAddition = 0

        LimeAddition_MassAl2O3 = (Cte_Al2O3 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaAl2O3 / 100) + (
                CalDol_knn * CalDolomitica_QuimicaAl2O3 / 100) +
                                  + ((FerroSi_gradoC_lf * FeSi_GC_QuimicaAl2O3 * (100- Rend_FeSi_GC)/ 10000) * Al_to_Al2O3) +
                                  + (Escoria_AlgranallaAddition * (
                                          100 - Parametro_Ox_Al) * AlBrique_Quimica_Al / 10000 * Al_to_Al2O3)
                                  + (Alalambre_lf * (
                        100 - Parametro_Ox_Al) * 100 / 10000 * Al_to_Al2O3) + Mass_Al2O3_total_tap)
        LimeAddition_MassCaO = (Cte_CaO + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaCaO / 100) +
                                (CalDol_knn * CalDolomitica_QuimicaCaO / 100) + Mass_CaO_total_tap)
        LimeAddition_MassSiO2 = (Cte_SiO2 + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaSiO / 100) +
                                 (CalDol_knn * CalDolomitica_QuimicaSiO / 100) + (((
                        FerroSi_gradoC_lf * FeSi_GC_QuimicaSi * (100 - Rend_FeSi_GC) / 10000)) * Si_to_SiO2) +
                                 ((SiliMgStd_gradoD_lf * SiMnSTD_QuimicaSi * (
                                         100 - Rend_SI_SiMnSTD) / 10000) * Si_to_SiO2) +
                                 (Escoria_AlgranallaAddition * (
                                         100 - Parametro_Ox_Al) * Al_Postas_Quimica_Si / 10000 * Si_to_SiO2) +
                                 Mass_SiO2_total_tap)
        LimeAddition_MassMgO = (Cte_MgO + (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaMgO / 100) +
                                (CalDol_knn * CalDolomitica_QuimicaMgO / 100) + Mass_MgO_total_tap)
        LimeAddition_MassP2O5 = Mass_P2O5_total_lf2
        LimeAddition_MassFe2O3 = (
                (Escoria_LimeAddition * CalSiderurgica_Gruesa_QuimicaFe2O3 / 100) + Mass_Fe2O3_total_tap)
        LimeAddition_MassMnO = Mass_MnO_total_lf2

        EscoriaMass_CalAjustado = (
                LimeAddition_MassAl2O3 + LimeAddition_MassCaO + LimeAddition_MassSiO2 + LimeAddition_MassMgO + LimeAddition_MassP2O5 + LimeAddition_MassFe2O3 + LimeAddition_MassMnO)

        # Final estimated slag composition with additions:
        Escoria_TotalMgO = float(100) * float(LimeAddition_MassMgO) / float(EscoriaMass_CalAjustado)
        Escoria_TotalCaO = float(100) * float(LimeAddition_MassCaO) / float(EscoriaMass_CalAjustado)
        Escoria_TotalAl2O3 = float(100) * float(LimeAddition_MassAl2O3) / float(EscoriaMass_CalAjustado)
        Escoria_TotalSiO2 = float(100) * float(LimeAddition_MassSiO2) / float(EscoriaMass_CalAjustado)
        Escoria_TotalMnO = float(100) * float(LimeAddition_MassMnO) / float(EscoriaMass_CalAjustado)
        Escoria_Total_Fe2O3 = 100 * LimeAddition_MassFe2O3 / EscoriaMass_CalAjustado
        Escoria_Total_P2O5 = 100 * LimeAddition_MassP2O5 / EscoriaMass_CalAjustado

        # Basicity cuaternaria:
        Escoria_BA4 = (float(Escoria_TotalCaO) + float(Escoria_TotalMgO)) / (
                float(Escoria_TotalSiO2) + float(Escoria_TotalAl2O3))

        Escoria_DeltaAl2O3 = float(Escoria_TotalAl2O3) - float(Escoria_TargetAl2O3)

    #Basicidad ternaria
    Escoria_BA3 = Escoria_TotalCaO / (Escoria_TotalSiO2 + Escoria_TotalAl2O3)

    #Relación CaO/Al2O3
    Relacion_CaOAl2O3 = Escoria_TotalCaO / Escoria_TotalAl2O3



    row.append(Escoria_TotalMassMgO)
    row.append(Escoria_TotalMassCaO)
    row.append(Escoria_TotalMassAl2O3)
    row.append(Escoria_TotalMassSiO2)
    row.append(Escoria_TotalMassMnO)
    row.append(Escoria_TotalMassFe2O3)
    row.append(Mass_P2O5_total_lf2)

    row.append(Escoria_TotalMgO)
    row.append(Escoria_TotalCaO)
    row.append(Escoria_TotalAl2O3)
    row.append(Escoria_TotalSiO2)
    row.append(Escoria_TotalMnO)
    row.append(Escoria_Total_Fe2O3)
    row.append(Escoria_Total_P2O5)
    row.append(Escoria_BA4)
    row.append(Escoria_BA3)
    row.append(Relacion_CaOAl2O3)
    a = Escoria_AlgranallaAddition
    row.append(a)
    B = Escoria_LimeAddition
    row.append(B)
    maria.append(Escoria_LimeAddition)
    maria.append(Escoria_AlgranallaAddition)
    maria.append(Escoria_BA3)
    maria.append(Relacion_CaOAl2O3)

n = int(len(row) / len(rows))  # int(len(row) / len(rows))
listalistas.append(row)

titulos = (['Mass_Al2O3_total_tap', 'Mass_CaO_total_ta', 'Mass_SiO2_total_tap', 'Mass_MnO_total_tap',
            'Mass_MgO_total_tap', 'Mass_P2O5_total_tap', 'Mass_Fe2O3_total_tap', 'Escoria_total_tap',
            'Porc_Al2O3_tap', 'Porc_CaO_tap', 'Porc_SiO_tap', 'Porc_MnO_tap', 'Porc_MgO_tap', 'Porc_P2O5_tap',
            'Porc_Fe2O3_tap', 'BA3_tap ','Relacion_CaOAl2O3_Tap',
            'CalDol_knn', 'CalSiderGruesa_knn', 'CalSiderFina_knn', 'AlBriqueta_knn', 'AlPosta_knn',
            'Mass_Al2O3_total_lf2', 'Mass_CaO_total_lf2', 'Mass_SiO2_total_lf2', 'Mass_MnO_total_lf2',
            'Mass_MgO_total_lf2',
            'Mass_P2O5_total_lf2', 'Mass_Fe2O3_total_lf2',
            'Escoria_total_lf2',
            'Porc_Al2O3_lf2', 'Porc_CaO_lf2', 'Porc_SiO_lf2', 'Porc_MnO_lf2', 'Porc_MgO_lf2', 'Porc_P2O5_lf2',
            'Porc_Fe2O3_lf2', 'BA3_lf2_new', 'Relacion_CaOAl2O3_lf2_new',
            'Escoria_totalMgO', 'Escoria_TotalMassCaO', 'Escoria_TotalMassAl2O3',
            'Escoria_TotalMassSiO2',
            'Escoria_TotalMassMnO', 'Escoria_TotalMassFe2O3', 'Escoria_totalMassP2O5 ',
            'Escoria_TotalMgO', 'Escoria_TotalCaO', 'Escoria_TotalAl2O3', 'Escoria_TotalSiO2', 'Escoria_TotalMnO',
            'Escoria_Total_Fe2O3', 'Escoria_Total_P2O5', 'Escoria_BA4', 'Escoria_BA3', 'Relacion CaO/Al2O3', 'Escoria_AlgranallaAdition',
            'EscoriaLimeAddition'])
listalistas = pd.DataFrame(np.array(row).reshape(len(rows), n), columns=titulos)
colnames_listalistas = pd.DataFrame(list(listalistas.columns))

#listalistas.to_excel('Frisa_8-07-20-6NN-Promedio-OptimizadosCaO58Al2O319.xlsx', engine='xlsxwriter')
maria=pd.DataFrame(np.array(maria).reshape(len(rows),9), columns=['CalDoloknn', 'CalSidergruesaknn', 'CalSiderfinaknn', 'Albriquetaknn', 'Alpostaknn', 'EscoriaLimeAdition', 'EscoriaAlAdition', 'BasicidadBA3','RelacionCaOAl2O3'])
import winsound
winsound.Beep(1000,700)
